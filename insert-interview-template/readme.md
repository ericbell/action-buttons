![insert interview template demo](insert-template.gif)

### Fields
* **[To-one relation]** Study
* **[Rich Text]** Notes
* **[Rich Text]** Interview Template (in Study)

### Config
Adjust the Type and Field names at the beginning of the code.