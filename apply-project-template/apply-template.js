const fibery = context.getService('fibery');
const schema = await fibery.getSchema();

/*
Configuration
 */
const TASK_TYPE_NAME = "Task";
const DO_NOT_COPY_FIELDS = ["References", "Files", "Documents", "Whiteboards"];
const COPY_PROJECT_RICH_TEXT_FIELDS = true;


/*
Auxiliary Functions
 */
function findRelation(fromTypeFull, toType) {
    const fields = schema.typeObjects.find(t => t.name === fromTypeFull).fieldObjects;
    const relationField = fields.find(f => f.type.endsWith(`/${toType}`));
    if (!relationField) {
        throw new Error(`Oops! The relation to ${TASK_TYPE_NAME} Type is missing.`)
    }
    return relationField;
}


function parseNames(projectTypeFull) {
    const [projectApp, projectType] = projectTypeFull.split('/');

    const taskTypeFull = findRelation(projectTypeFull, TASK_TYPE_NAME).type;
    const [taskApp, taskType] = taskTypeFull.split('/');

    const projectTemplateTypeFull = findRelation(projectTypeFull, `${projectType} Template`).type;
    const [projectTemplateApp, projectTemplateType] = projectTemplateTypeFull.split('/');

    const taskTemplatesField = findRelation(projectTemplateTypeFull, `${taskType} Template`);

    return {
        projectApp: projectApp,
        projectType: projectType,
        projectTypeFull: projectTypeFull,

        taskApp: taskApp,
        taskType: taskType,
        taskTypeFull: taskTypeFull,

        projectTemplateApp: projectTemplateApp,
        projectTemplateType: projectTemplateType,
        projectTemplateTypeFull: projectTemplateTypeFull,

        taskTemplateType: `${taskType} Template`,
        taskTemplateTypeFull: taskTemplatesField.type,
        taskTemplatesField: taskTemplatesField.title
    }
}

function getTemplateFields(taskTemplateTypeFullName, taskTypeFullName) {
    const taskTemplateFields = schema.typeObjectsByName[taskTemplateTypeFullName].fieldObjects;
    const taskFields = schema.typeObjectsByName[taskTypeFullName].fieldObjects;

    const areFieldsTheSame = (tf, f) => f.title === tf.title && (
        (f.type === tf.type) ||
        (f.typeObject.isEnum && tf.typeObject.isEnum && f.cardinality === tf.cardinality) ||
        (f.type === taskTypeFullName && tf.type === taskTemplateTypeFullName && f.cardinality === tf.cardinality)
    )

    return taskTemplateFields.filter(tf =>
        taskFields.some(f => !f.isReadOnly && areFieldsTheSame(tf, f)) &&
        !DO_NOT_COPY_FIELDS.includes(tf.title));
}

function buildTask(templateFields, templateTask) {
    return templateFields.reduce((acc, f) => {
        if (f.type === "fibery/Button" || f.type === "Collaboration~Documents/Document" || f.isReadOnly) {
            return acc;
        } else if (f.typeObject.isPrimitive) {
            return { ...acc, [f.title]: templateTask[f.title] };
        } else if (f.typeObject.isEnum && f.cardinality === ":cardinality/many-to-one") {
            // Workaround for issue with setting null to enums
            const enumSetter = templateTask[f.title].Name ? { [f.title]: templateTask[f.title].Name } : {};
            return { ...acc, ...enumSetter };
        } else if (f.typeObject.isPrimitive === false && f.cardinality === ":cardinality/many-to-one") {
            return { ...acc, [f.title]: templateTask[f.title].id };
        } else {
            return acc;
        }
    }, {});
}

// Simplify when secret is available instead of id
async function copyRichText(templateDocumentSecret, targetDocumentSecret, appendIfNotEmpty = false) {
    const templateContent = await fibery.getDocumentContent(templateDocumentSecret, "json");
    if (!templateContent) {
        return;
    }

    if (!appendIfNotEmpty) {
        return await fibery.setDocumentContent(targetDocumentSecret, templateContent, "json");
    } else {
        const currentContent = await fibery.getDocumentContent(targetDocumentSecret, "md");
        const method = currentContent ? "appendDocumentContent" : "setDocumentContent";
        return await fibery[method](targetDocumentSecret, templateContent, "json");
    }
}


/*
The Real Deal
 */

const names = parseNames(args.currentEntities[0].type);

const projects = args.currentEntities.filter(e => e[names.projectTemplateType].id);
if (!projects.length) {
    const article = args.currentEntities.length === 1 ? "the" : "each";
    throw new Error(`Please pick a ${names.projectTemplateType} for ${article} ${names.projectType}`);
}


const projectRichTextFields = getTemplateFields(names.projectTemplateTypeFull, names.projectTypeFull)
    .filter(tf => tf.type === "Collaboration~Documents/Document");

const taskTemplateFields = getTemplateFields(names.taskTemplateTypeFull, names.taskTypeFull);
const taskRichTextFields = taskTemplateFields.filter(tf => tf.type === "Collaboration~Documents/Document");
const taskDependencyField = taskTemplateFields.find(tf => tf.type === names.taskTemplateTypeFull);


for (const project of projects) {
    const projectTemplate = await fibery.getEntityById(names.projectTemplateTypeFull, project[names.projectTemplateType].id,
        [names.taskTemplatesField, ...projectRichTextFields.map(rtf => rtf.title)]);

    if (COPY_PROJECT_RICH_TEXT_FIELDS) {
        for (const richTextField of projectRichTextFields) {
            await copyRichText(projectTemplate[richTextField.title].secret, project[richTextField.title].secret, true);
        }
    }

    const taskTemplateIds = projectTemplate[names.taskTemplatesField].map(tt => tt.id);
    const taskTemplates = await fibery.getEntitiesByIds(names.taskTemplateTypeFull, taskTemplateIds,
        taskTemplateFields.map(tf => tf.title)
    );

    const tasks = await Promise.all(taskTemplates.map(async (taskTemplate) => {
        const task = await fibery.createEntity(names.taskTypeFull, {
            ...buildTask(taskTemplateFields, taskTemplate),
            [names.projectType]: project.id,
            [names.taskTemplateType]: taskTemplate.id,
            "Created By": args.currentUser.id
        });

        if (taskTemplate["Assignees"] && taskTemplate["Assignees"].length) {
            for (const assignee of taskTemplate["Assignees"]) {
                await fibery.assignUser(names.taskTypeFull, task.id, assignee.id);
            }
        }

        for (const richTextField of taskRichTextFields) {
            await copyRichText(taskTemplate[richTextField.title].secret, task[richTextField.title].secret);
        }

        return task;
    }));

    if (taskDependencyField) {
        const templateToTask = taskTemplates.reduce((acc, tt, i) => ({
            ...acc,
            [tt.id]: tasks[i].id
        }), {});

        for (const taskTemplate of taskTemplates) {
            const taskId = templateToTask[taskTemplate.id];
            for (const dependency of taskTemplate[taskDependencyField.title]) {
                const blockerTaskId = templateToTask[dependency.id];
                await fibery.addCollectionItem(names.taskTypeFull, taskId, taskDependencyField.title, blockerTaskId);
            }
        }
    }
}

return "✅ Done"